package org.bitbucket.jorool1988.server;

import java.math.BigInteger;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;

import org.bitbucket.jorool1988.comum.Resultado;
import org.bitbucket.jorool1988.comum.TipoJogada;

public interface JokenpoServer extends Remote {

	int clientesConectados() throws RemoteException;
	
	void conectarCliente(String id) throws RemoteException;

	Resultado registrarJogada(String id, TipoJogada tipoJogada, BigInteger hash) throws RemoteException, NoSuchAlgorithmException;

	void aguardarInicioJogo(String id) throws RemoteException;

	void iniciarJogo(String id) throws RemoteException;
	
}
