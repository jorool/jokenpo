package org.bitbucket.jorool1988.server;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import org.bitbucket.jorool1988.comum.Jogada;
import org.bitbucket.jorool1988.comum.Jogador;
import org.bitbucket.jorool1988.comum.Resultado;
import org.bitbucket.jorool1988.comum.TipoJogada;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class JokenpoServerImpl implements JokenpoServer, Serializable {

	private static final long serialVersionUID = 1L;

	private static Map<String, Jogador> jogadores = Maps.newHashMap();

	private static List<Jogada> jogadas = Lists.newLinkedList();

	private static Object lock = new Object();

	public static Logger logger = Logger.getLogger(JokenpoServerImpl.class.getName());
	public static FileHandler fileHandler;

	private JokenpoServerImpl() {
	}

	public static JokenpoServerImpl newInstance() {
		return new JokenpoServerImpl();
	}

	public static void main(String[] args) throws AlreadyBoundException, SecurityException, IOException {
		fileHandler = new FileHandler("jokenpo.log", true);
		logger.addHandler(fileHandler);
		logger.info("INICIANDO JOKENPO SERVER");
		JokenpoServerImpl server = JokenpoServerImpl.newInstance();
		JokenpoServer stub = (JokenpoServer) UnicastRemoteObject.exportObject(server, 0);

		Registry registry = LocateRegistry.createRegistry(1099);
		registry.bind("stub", stub);

		logger.info("JOKENPO SERVER INICIADO");
	}

	@Override
	public int clientesConectados() throws RemoteException {
		return jogadores.size();
	}

	@Override
	public void conectarCliente(String id) throws RemoteException {
		jogadores.put(id, Jogador.newInstance(id));
		logger.info(String.format("NOVO JOGADOR CONECTADO: %s", id));
	}

	@Override
	public Resultado registrarJogada(String id, TipoJogada tipoJogada, BigInteger hash) throws RemoteException, NoSuchAlgorithmException {
		/**
		 * CONSISTENCIA: é feita a verificacao do hash code da jogada enviada ao
		 * servidor para garantir a integridade do dado enviado
		 */
		if (!tipoJogada.geraHash(id, tipoJogada).equals(hash)) {
			logger.warning("ERRO DE CONSISTENCIA");
			return Resultado.ERRO_DE_CONSISTENCIA;
		}
		
		Jogador jogador = retornaJogador(id);
		Jogada jogadaAtual = Jogada.newInstance(tipoJogada);

		logger.info(String.format("JOGADOR %s JOGOU %s", jogador.toString(), jogadaAtual.toString()));
		jogador.addJogada(jogadaAtual);

		Jogada jogadaOponente;
		if (apenasUmJogadorJogou()) {
			// caso que o outro jogador ja fez jogada
			jogadaOponente = getUltimaJogada();
			jogadas.add(jogadaAtual);
			notificarOponente();
		}
		else {
			// caso que o jogador atual foi o primeiro a jogar
			jogadas.add(jogadaAtual);
			aguardarOponenteJogar();
			jogadaOponente = getUltimaJogada();
		}
		return getResultado(jogadaAtual, jogadaOponente);
	}

	private Jogada getUltimaJogada() {
		return jogadas.get(jogadas.size() - 1);
	}

	private void aguardarOponenteJogar() {
		synchronized (lock) {
			while (apenasUmJogadorJogou()) {
				try {
					lock.wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean apenasUmJogadorJogou() {
		// sabemos que os dois jogadores jogaram
		// quando o jogadas.size() for multiplo de 2
		return jogadas.size() % 2 == 1;
	}

	private void notificarOponente() {
		synchronized (lock) {
			lock.notify();
		}
	}

	private Resultado getResultado(Jogada jogadaAtual, Jogada jogadaOponente) {
		return jogadaAtual.getTipoJogada().getResultado(jogadaOponente.getTipoJogada());
	}

	private Jogador retornaJogador(String id) {
		Jogador jogador = jogadores.get(id);
		if (jogador != null) {
			return jogador;
		}
		return Jogador.newInstance("");
	}

	@Override
	public void aguardarInicioJogo(String id) throws RemoteException {
		Jogador jogador = retornaJogador(id);
		logger.info(String.format("JOGADOR %s ESTÁ AGUARDANDO OPONENTE..", jogador));
		synchronized (lock) {
			while (clientesConectados() < 2) {
				try {
					lock.wait();
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void iniciarJogo(String id) throws RemoteException {
		Jogador jogador = retornaJogador(id);
		notificarOponente();
		logger.info(String.format("JOGADOR %s CHEGOU. JOGO PODE COMECAR.", jogador));
	}

}