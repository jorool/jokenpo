package org.bitbucket.jorool1988.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.NoSuchAlgorithmException;

import org.bitbucket.jorool1988.comum.Resultado;
import org.bitbucket.jorool1988.comum.TipoJogada;
import org.bitbucket.jorool1988.server.JokenpoServer;

public class JokenpoClient {

	private static Registry registry;

	private static JokenpoServer jokenpoServer;

	private static String processo;

	private JokenpoClient() {
	}

	public static JokenpoClient newInstance() {
		return new JokenpoClient();
	}

	public static void main(String[] args) throws RemoteException, InterruptedException, NoSuchAlgorithmException {
		System.out.println("INICIANDO JOKENPO CLIENT");

		processo = ManagementFactory.getRuntimeMXBean().getName();

		iniciar();
	}

	private static void iniciar() throws InterruptedException, RemoteException, NoSuchAlgorithmException {
		while (!conectaServidor()) {
			System.out.println("NAO CONECTADO, TENTANDO NOVAMENTE");
			Thread.sleep(2000);
		}
		inicar();
	}

	private static void inicar() throws RemoteException, NoSuchAlgorithmException, InterruptedException {
		verificarInicioJogo();
		jogar();
	}
	
	private static void verificarInicioJogo() throws RemoteException {
		int clientesConectados = jokenpoServer.clientesConectados();

		System.out.println("VERIFICANDO OPONENTE");
		System.out.printf("OPONENTES CONECTADOS: %d\n", clientesConectados);

		if (clientesConectados > 1){
			System.out.println("INICIAR JOGO..");
			jokenpoServer.iniciarJogo(processo);
		} else {
			System.out.println("AGUARDANDO OPONENTE CHEGAR..");
			jokenpoServer.aguardarInicioJogo(processo);
			System.out.println("OPONENTE CHEGOU. JOGO PODE COMECAR.");
		}
	}

	private static void jogar() throws NoSuchAlgorithmException, RemoteException, InterruptedException {
		InputStreamReader istream = new InputStreamReader(System.in);
		BufferedReader bufRead = new BufferedReader(istream);
		String jogada = "";
		while (!jogadaValida(jogada)) {
			try {
				System.out.print("INFORME A JOGADA (P: PEDRA, A: PAPEL, T: TESOURA): ");
				jogada = bufRead.readLine();
			} catch (IOException err) {
				err.printStackTrace();
			}
		}
		// enviar jogada e receber resultado
		/**
		 * TOLERANCIA A FALHA - RECUPERACAO: erros podem ocorrer internamente no servidor ou
		 * durante o envio da jogada. mesmo se ocorrer um erro, o cliente se mantem funcionando.
		 * recuperacao pq o processo eh reiniciado
		 */
		try {
			System.out.println("JOGADA ENVIADA AO SERVIDOR. AGUARDANDO RESULTADO");
			TipoJogada tipoJogada = TipoJogada.fromCodigo(jogada.toUpperCase());
			Resultado resultado = jokenpoServer.registrarJogada(processo, tipoJogada, tipoJogada.geraHash(processo, tipoJogada));
			System.out.println(String.format("RESULTADO: %s", resultado));
			jogar();
		} catch (RemoteException e) {
			System.err.println("ERRO AO ENVIAR JOGADA, RECONECTANDO");
			iniciar();
		}
	}

	private static boolean conectaServidor() {
		/**
		 * TOLERANCIA A FALHA - MASCARAMENTO: ao tentar conectar num servidor nao instanciado,
		 * o cliente mascara a excecao e continua tentando a conexao
		 */
		System.out.println("CONECTANDO AO SERVIDOR");
		try {
			registry = LocateRegistry.getRegistry(1099);
			jokenpoServer = (JokenpoServer) registry.lookup("stub");
			jokenpoServer.conectarCliente(processo);
			System.out.println("CONECTADO");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private static boolean jogadaValida(String jogada) {
		if (TipoJogada.fromCodigo(jogada.toUpperCase()) != null) {
			return true;
		}
		return false;
	}
	
}