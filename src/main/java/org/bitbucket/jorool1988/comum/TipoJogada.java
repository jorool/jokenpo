package org.bitbucket.jorool1988.comum;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public enum TipoJogada {

	PEDRA("P") {
		@Override
		public Resultado getResultado(TipoJogada outro) {
			if (outro.equals(TESOURA)) {
				return Resultado.VENCEU;
			}
			else if (outro.equals(PEDRA)) {
				return Resultado.EMPATOU;
			}
			return Resultado.PERDEU;
		}
	},
	PAPEL("A") {
		@Override
		public Resultado getResultado(TipoJogada outro) {
			if (outro.equals(PEDRA)) {
				return Resultado.VENCEU;
			}
			else if (outro.equals(PAPEL)) {
				return Resultado.EMPATOU;
			}
			return Resultado.PERDEU;
		}
	},
	TESOURA("T") {
		@Override
		public Resultado getResultado(TipoJogada outro) {
			if (outro.equals(PAPEL)) {
				return Resultado.VENCEU;
			}
			else if (outro.equals(TESOURA)) {
				return Resultado.EMPATOU;
			}
			return Resultado.PERDEU;
		}
	};

	private static final Map<String, TipoJogada> valueMap;

	static {
		Builder<String, TipoJogada> builder = ImmutableMap.builder();

		for (TipoJogada tipoJogada : values()) {
			builder.put(tipoJogada.codigo, tipoJogada);
		}

		valueMap = builder.build();
	}

	public BigInteger geraHash(String id, TipoJogada tipoJogada) throws NoSuchAlgorithmException {
		String s = String.format("%s %s", id, tipoJogada.toString());
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.update(s.getBytes(), 0, s.length());
		return new BigInteger(1, m.digest());
	}

	public static TipoJogada fromCodigo(String codigo) {
		return valueMap.get(codigo);
	}

	TipoJogada(String codigo) {
		this.codigo = codigo;
	}

	public abstract Resultado getResultado(TipoJogada outro);

	private final String codigo;

	public String getCodigo() {
		return codigo;
	}

}
