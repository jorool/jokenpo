package org.bitbucket.jorool1988.comum;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class Jogador {

	private final String id;

	private final List<Jogada> jogadas;

	private Jogador(String id, List<Jogada> jogadas) {
		this.id = id;
		this.jogadas = jogadas;
	}

	public static Jogador newInstance(String id) {
		checkArgument(!Strings.isNullOrEmpty(id), "id nao pode ser nulo nem vazio");
		List<Jogada> jogadas = Lists.newLinkedList();
		return new Jogador(id, jogadas);
	}

	public static Jogador newInstance(String id, List<Jogada> jogadas) {
		checkArgument(!Strings.isNullOrEmpty(id), "id nao pode ser nulo nem vazio");
		checkNotNull(jogadas, "jogadas nao pode ser nulo");
		return new Jogador(id, jogadas);
	}

	public Jogador addJogada(Jogada jogada) {
		if (!this.jogadas.contains(jogada)) {
			this.jogadas.add(jogada);
		}
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Jogador) {
			Jogador other = (Jogador) obj;
			return Objects.equal(this.id, other.id) && Objects.equal(this.jogadas, other.jogadas);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id, jogadas);
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("jogadas", jogadas).toString();
	}

	public String getId() {
		return id;
	}

	public List<Jogada> getJogadas() {
		return jogadas;
	}

}
