package org.bitbucket.jorool1988.comum;

public enum Resultado {

	VENCEU,
	PERDEU,
	EMPATOU,
	ERRO_DE_CONSISTENCIA;
	
}
