package org.bitbucket.jorool1988.comum;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Date;

import com.google.common.base.Objects;

public class Jogada {

	private final TipoJogada tipoJogada;

	private final Date hora;

	private Jogada(TipoJogada tipoJogada, Date hora) {
		this.tipoJogada = tipoJogada;
		this.hora = hora;
	}
	
	public static Jogada newInstance(TipoJogada tipoJogada) {
		checkNotNull(tipoJogada, "tipoJogada nao pode ser nulo");
		return new Jogada(tipoJogada, new Date());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Jogada) {
			Jogada other = (Jogada) obj;
			return Objects.equal(this.tipoJogada, other.tipoJogada) && Objects.equal(this.hora, other.hora);
		}
		return false;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("tipo jogada", this.tipoJogada).add("hora", hora).toString();
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(tipoJogada, hora);
	}

	public TipoJogada getTipoJogada() {
		return tipoJogada;
	}

	public Date getHora() {
		return hora;
	}

}
